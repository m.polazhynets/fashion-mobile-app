export const IMAGES = {
  BG_CATEGORY: require("../../assets/images/bg_category.png"),
  BG_NOVELTIES: require("../../assets/images/bg_novelties.png"),
  BG_SMALL: require("../../assets/images/bg_small.png"),
  PRODUCT1: require("../../assets/images/products/product1.png"),
  PRODUCT2: require("../../assets/images/products/product2.png"),
  PRODUCT3: require("../../assets/images/products/product3.png"),
  PRODUCT4: require("../../assets/images/products/product4.png"),
};
  
export const ICONS = {
	LIKE: require("../../assets/icons/like/like.png"),
  RED_LIKE: require("../../assets/icons/red-like/red-like.png"),
  ARROW_BACK: require("../../assets/icons/arrow-back/arrow-back.png"),
  SHARE: require("../../assets/icons/share/share.png"),
  ARROW_LEFT: require("../../assets/icons/arrows/left.png"),
  ARROW_RIGHT: require("../../assets/icons/arrows/right.png"),
};
  