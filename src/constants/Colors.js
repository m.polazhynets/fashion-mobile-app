export const COLORS = {
	BLACK: "#000",
	BLACK_LIGHT: "#202020",
	WHITE: "#fff",
	LIGHT_GREY: "#BABABA",
	BG_BUTTON: "rgba(32, 32, 32, 0.2)",
	BEIGE: 'rgba(220, 189, 144, 1)',
	GREY: "#ADB3BB"
};
  