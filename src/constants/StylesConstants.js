import { Dimensions } from "react-native";

const { width: screenWidth, height: screenHeight } = Dimensions.get("window");
const localWidth = screenWidth >= screenHeight ? screenHeight : screenWidth;
const localHeight = screenWidth < screenHeight ? screenHeight : screenWidth;
const widthCoef = (localWidth > 375 ? 375 : localWidth) / 375;
const heightCoef = (localHeight > 812 ? 812 : localHeight) / 812;

export const scale = (size) => widthCoef * size;
export const vScale = (size) => heightCoef * size;
export const deviceWidth = screenWidth;
export const deviceHeight = screenHeight;

const vars = {
  fontSize: {
    // smalXS: scale(11),
    // smallS: scale(12),
    // smallL: scale(14),
    // smallXL: scale(15),

    // mediumS: scale(16),
    // mediumL: scale(18),
    // mediumXL: scale(20),

    // largeS: scale(22),
    // largeM: scale(24),
    // largeL: scale(28),

    // extraLargeM: scale(32),
    // extraLargeL: scale(40),
    // extraLargeXL: scale(46),

    smallS: scale(12),
    smallL: scale(14),

    mediumS: scale(16),
    mediumXL: scale(20),

    largeS: scale(22),
    largeM: scale(24),

  },
};
export const FONT_FAMILY = {
  MONTSERRAT_REGULAR: "Montserrat-Regular",
  MONTSERRAT_MEDIUM: "Montserrat-Medium",
  MONTSERRAT_SEMI_BOLD: "Montserrat-SemiBold",
  MONTSERRAT_BOLD: "Montserrat-Bold",
};

export default vars;
