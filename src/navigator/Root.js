import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import { AppFlowKeys } from "./Keys";
import { MainStack, SearchStack } from "./stacks";
import { HomeIcon, SearchIcon, ProfileIcon } from '../components/Icons';
import { COLORS } from "../constants/Colors";
import { vScale } from "../constants/StylesConstants";

const Tab = createBottomTabNavigator();

const RootStackScreen = () => {
  return (
      <Tab.Navigator
        initialRouteName="Home"
        screenOptions={() => ({
          headerShown: false,
          tabBarStyle: {
            height: vScale(68),
          },
          tabBarLabel:() => {return null},
        })}
      >
        <Tab.Screen
          name={AppFlowKeys.Main}
          component={MainStack}
          options={({route}) => ({
            tabBarIcon: ({ focused }) => (
              <HomeIcon color={focused ? COLORS.BLACK_LIGHT : COLORS.LIGHT_GREY} />
            ),
            tabBarStyle: ((route) => {
              if (getFocusedRouteNameFromRoute(route) == "Product") {
                return { display: "none" };
              }
              return { display: "flex" };
            })(route)
          })}
        />
        <Tab.Screen
          name={AppFlowKeys.Search}
          component={SearchStack}
          options={() => ({
            tabBarIcon: ({ focused }) => (
              <SearchIcon color={focused ? COLORS.BLACK_LIGHT : COLORS.LIGHT_GREY} />
            ),
          })}
        />
        <Tab.Screen
          name={AppFlowKeys.Profile}
          component={MainStack}
          options={() => ({
            tabBarIcon: ({ focused }) => (
              <ProfileIcon color={focused ? COLORS.BLACK_LIGHT : COLORS.LIGHT_GREY} />
            )
          })}
        />
      </Tab.Navigator>
  );
};

export default RootStackScreen;
