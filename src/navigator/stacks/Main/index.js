import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import { Home, Product } from "../../../screens";
import { MainFlowKeys } from "../../Keys";

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
      }}
    >
      <Stack.Screen 
        name={MainFlowKeys.Home} 
        component={Home} />
      <Stack.Screen 
        name={MainFlowKeys.Product} 
        component={Product}/>
    </Stack.Navigator>
  );
};

export default MainStack;
