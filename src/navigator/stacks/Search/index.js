import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import { Search } from "../../../screens";
import { SearchFlowKeys } from "../../Keys";

const Stack = createNativeStackNavigator();

const SearchStack = () => {
  return (
    <Stack.Navigator
      initialRouteName={SearchFlowKeys.Search}
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
      }}
    >
      <Stack.Screen name={SearchFlowKeys.Search} component={Search} />
    </Stack.Navigator>
  );
};

export default SearchStack;
