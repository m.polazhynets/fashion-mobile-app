export { default as Home } from "./Home";
export { default as Product } from "./Product";
export { default as Search } from "./Search";
