import React, {useState} from "react";
import { ImageBackground, View, ScrollView, Text, TouchableOpacity } from "react-native";
import ReadMore from 'react-native-read-more-text'

import { IMAGES, ICONS } from "../../constants/Images";
import { AroundButton, Button } from "../../components";
import styles from "./styles.js";

const Product = ({route, navigation}) => {
	const [size, setSize] = useState('S')
	const {item: {subtitle, title, price, description}} =  route.params

	const renderTruncatedFooter = (handlePress) => {
		return (
      <Text style={styles.readMore} onPress={handlePress}>
        Read more
      </Text>
    );
	}

	const renderRevealedFooter = (handlePress) => {
		return (
      <Text style={styles.readMore} onPress={handlePress}>
        Show less
      </Text>
    );
	}

  return (
    <ScrollView style={styles.container}>
      <ImageBackground
				style={styles.image}
				source={IMAGES.BG_SMALL}
				resizeMode="cover"
			>
				<View style={styles.buttons}>
					<AroundButton icon={ICONS.ARROW_BACK} onPress={() => navigation.goBack()}/>
					<AroundButton icon={ICONS.SHARE} onPress={() => {}}/>
				</View>
			</ImageBackground>
			<View style={styles.wrapper}>
				<Text style={styles.subtitle}>{subtitle}</Text>
				<Text style={styles.title}>{title}</Text>
				<Text style={styles.price}>${price}</Text>
				<Text style={styles.sizes}>Choose size</Text>
				<View style={styles.sizeWrapper}>
					<TouchableOpacity 
						onPress={() => setSize('S')} 
						style={size === 'S' ? styles.size : {...styles.size, ...styles.activeSize}}>
						<Text style={size === 'S' ? styles.sizeText : {...styles.sizeTaxt, ...styles.activeSizeText}}>S</Text>
					</TouchableOpacity>
					<TouchableOpacity 
						onPress={() => setSize('M')} 
						style={size === 'M' ? styles.size : {...styles.size, ...styles.activeSize}}>
						<Text style={size === 'M' ? styles.sizeText : {...styles.sizeTaxt, ...styles.activeSizeText}}>M</Text>
					</TouchableOpacity>
					<TouchableOpacity 
						onPress={() => setSize('L')} 
						style={size === 'L' ? styles.size : {...styles.size, ...styles.activeSize}}>
						<Text style={size === 'L' ? styles.sizeText : {...styles.sizeTaxt, ...styles.activeSizeText}}>L</Text>
					</TouchableOpacity>
					<TouchableOpacity 
						onPress={() => setSize('XL')} 
						style={size === 'XL' ? styles.size : {...styles.size, ...styles.activeSize}}>
						<Text style={size === 'XL' ? styles.sizeText : {...styles.sizeTaxt, ...styles.activeSizeText}}>XL</Text>
					</TouchableOpacity>
				</View>
				<Text  style={styles.descriptions}>Description</Text>
				<ReadMore
					numberOfLines={2}
					renderTruncatedFooter={renderTruncatedFooter}
          renderRevealedFooter={renderRevealedFooter}
				>
					<Text style={styles.description}>{description}</Text>
				</ReadMore>
				<View style={styles.bottomButtons}>
					<Button title='Pre-order'/>
					<TouchableOpacity style={styles.button}>
						<Text style={styles.buttonText}>Send as gift</Text>
					</TouchableOpacity>
				</View>
			</View>
    </ScrollView>
  );
};

export default Product;
