import { StyleSheet } from "react-native";
import vars, {
  vScale,
  scale,
  FONT_FAMILY,
} from "../../constants/StylesConstants";
import { COLORS } from "../../constants/Colors";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.WHITE
  },
  image: {
		height: vScale(370),
		paddingTop: vScale(54),
		paddingHorizontal: scale(20)
  },
	buttons: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	wrapper: {
		paddingHorizontal: scale(20)
	},
	subtitle: {
		color: COLORS.GREY,
    fontSize: vars.fontSize.smallS,
    fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
		marginTop: vScale(15)
	},
	title: {
		marginTop: vScale(8),
    fontSize: vars.fontSize.largeM,
    fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
		lineHeight: vScale(32),
		color: COLORS.BLACK_LIGHT
	},
	price: {
		marginTop: vScale(12),
		fontSize: vars.fontSize.mediumXL,
    fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
		color: COLORS.BEIGE
	},
	sizes: {
		marginTop: vScale(24),
		fontSize: vars.fontSize.mediumS,
    fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
		color: COLORS.BLACK_LIGHT
	},
	sizeWrapper: {
		flexDirection: 'row',
		marginTop: vScale(12),
	},
	size: {
		width: scale(32),
		height: scale(32),
		backgroundColor: COLORS.BLACK_LIGHT,
		justifyContent: 'center',
		alignItems: 'center',
		marginRight: scale(8),
		borderRadius: scale(16),
	},
	sizeText: {
		color: COLORS.WHITE,
		fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
		fontSize: vars.fontSize.smallL,
	},
	activeSize:{
		backgroundColor: COLORS.WHITE
	},
	activeSizeText: {
		color: COLORS.BLACK_LIGHT
	},
	descriptions: {
		fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
		fontSize: vars.fontSize.mediumS,
		color: COLORS.BLACK_LIGHT,
		marginTop: vScale(24),
	},
	description: {
		color: COLORS.GREY,
    fontSize: vars.fontSize.smallL,
    fontFamily: FONT_FAMILY.MONTSERRAT_REGULAR,
		marginTop: vScale(8),
		lineHeight: vScale(24)
	},
	bottomButtons: {
		marginTop: vScale(37),
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: vScale(32)
	},
	button: {
		justifyContent: 'center',
		paddingRight: scale(30)
	},
	buttonText: {
		fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
		fontSize: vars.fontSize.mediumS,
		color: COLORS.BLACK_LIGHT,
	},
	readMore: {
		fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
		fontSize: vars.fontSize.smallL,
		color: COLORS.BLACK_LIGHT,
	}
});
