import { StyleSheet } from "react-native";
import vars, {
  vScale,
  scale,
  FONT_FAMILY,
} from "../../constants/StylesConstants";
import { COLORS } from "../../constants/Colors";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    paddingBottom: vScale(150)
  },
  wrapper: {
    paddingHorizontal: scale(20),
    paddingTop: vScale(54),
  },
  title: {
    fontSize: vars.fontSize.largeS,
    fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
  },
  subtitle: {
    color: COLORS.LIGHT_GREY,
    fontSize: vars.fontSize.smallS,
    fontFamily: FONT_FAMILY.MONTSERRAT_REGULAR,
    marginTop: vScale(4)
  },
  categoriesList: {
    marginTop: vScale(20),
  },
  noveltiesList: {
    marginTop: vScale(24)
  }
});
