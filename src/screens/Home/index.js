import React from "react";
import { Text, View, FlatList } from "react-native";

import {CategoriesItem, NoveltiesItem, GiftModal} from "../../components";
import { MainFlowKeys } from "../../navigator/Keys";
import styles from "./styles.js";

const categoriesList = [
	{
		id: 1,
		title: 'Gift For Him'
	},
	{
		id: 2,
		title: 'Gift For Him'
	},
	{
		id: 3,
		title: 'Gift For Him'
	}
];

const noveltiesList = [
	{
		id: 1,
		title: 'Short Black Crew Neck T-Shirt',
		subtitle: 'Product will be launched in 12h 53m',
		price: '20.21',
		description: 'Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more'
	},
	{
		id: 2,
		title: 'Short Black Crew Neck T-Shirt',
		subtitle: 'Product will be launched in 12h 53m',
		price: '20.21',
		description: 'Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more'
	},
	{
		id: 3,
		title: 'Short Black Crew Neck T-Shirt',
		subtitle: 'Product will be launched in 12h 53m',
		price: '20.21',
		description: 'Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more'
	}
]

const Home = ({navigation}) => {
	const selectCategories = (id) => {
		console.log(id,'id')
	}
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
				<Text style={styles.title}>New Products</Text>
				<Text style={styles.subtitle}>The products will be launched this month</Text>
				<FlatList
					horizontal
					showsHorizontalScrollIndicator={false}
					style={styles.categoriesList}
					data={categoriesList}
					keyExtractor={item => item.id}
					renderItem={({item}) =>
						<CategoriesItem 
							item={item}
							onPress={() => selectCategories(item.id)}/>
					}
				/>
				<FlatList
					showsVerticalScrollIndicator={false}
					style={styles.noveltiesList}
					data={noveltiesList}
					keyExtractor={item => item.id}
					renderItem={({item}) =>
						<NoveltiesItem 
							item={item}
							onPress={() =>
								navigation.navigate(MainFlowKeys.Product, {
									item: item,
								})
							}
						/>
					}
				/>	
			</View>
			{/* <GiftModal/> */}
    </View>
  );
};

export default Home;
