import { StyleSheet } from "react-native";
import vars, {
  vScale,
  scale,
  FONT_FAMILY,
} from "../../constants/StylesConstants";
import { COLORS } from "../../constants/Colors";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.WHITE
  },
  wrapper: {
		paddingHorizontal: scale(20),
		paddingTop: vScale(57)
	},
	header: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	buttons: {
		flexDirection: 'row'
	},
	button: {
		marginLeft: scale(8)
	},
	month: {
		fontSize: vars.fontSize.smallL,
		color: COLORS.BLACK_LIGHT,
		fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD
	},
	days: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingHorizontal: scale(8),
		marginTop: vScale(17),
		marginBottom: vScale(10),
	},
	day: {
		alignItems: 'center'
	},
	dayName: {
		fontSize: vars.fontSize.smallL,
		color: COLORS.GREY,
		fontFamily: FONT_FAMILY.MONTSERRAT_REGULAR
	},
	dayNumber: {
		marginTop: scale(8),
		width: scale(40),
		height: scale(40),
		backgroundColor: COLORS.WHITE,
		borderRadius: scale(20),
		justifyContent: 'center',
		alignItems: 'center'
	},
	activeDayNumber: {
		backgroundColor: COLORS.BEIGE,
	},
	dayNumberText:{
		fontSize: vars.fontSize.smallL,
		color: COLORS.BLACK_LIGHT,
		fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
	},
	activeDayNumberText: {
		color: COLORS.WHITE,
		fontSize: vars.fontSize.smallL,
		fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
	},
	afterDayNumberText: {
		color: COLORS.GREY,
		fontSize: vars.fontSize.smallL,
		fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
	},
	productsContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		marginTop: vScale(20),
		paddingBottom: vScale(120)
	}
});
