import React from "react";
import { View, FlatList, Text, Image, TouchableOpacity } from "react-native";
import moment from 'moment'

import { IMAGES, ICONS } from "../../constants/Images";
import { ProductItem } from "../../components";
import { MainFlowKeys } from "../../navigator/Keys";
import styles from "./styles.js";

const days = [
	{id: 7, name: 'Mon'}, 
	{id: 6, name: 'Tue'}, 
	{id: 5, name: 'Wed'}, 
	{id: 4, name: 'Thu'}, 
	{id: 3, name: 'Fri'}, 
	{id: 2, name: 'Sat'}, 
	{id: 1, name: 'Sun'}
];

const products = [
	{
		id: 1,
		title: 'Short Black Crew Neck T-Shirt',
		price: '20.21',
		img: IMAGES.PRODUCT1,
		subtitle: 'Product will be launched in 12h 53m',
		price: '20.21',
		description: 'Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more'
	},
	{
		id: 2,
		title: 'Short Black Crew Neck T-Shirt',
		price: '20.21',
		img: IMAGES.PRODUCT2,
		subtitle: 'Product will be launched in 12h 53m',
		price: '20.21',
		description: 'Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more'
	},
	{
		id: 3,
		title: 'Short Black Crew Neck T-Shirt',
		price: '20.21',
		img: IMAGES.PRODUCT3,
		subtitle: 'Product will be launched in 12h 53m',
		price: '20.21',
		description: 'Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more'
	},
	{
		id: 4,
		title: 'Short Black Crew Neck T-Shirt',
		price: '20.21',
		img: IMAGES.PRODUCT4,
		subtitle: 'Product will be launched in 12h 53m',
		price: '20.21',
		description: 'Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more'
	},
	{
		id: 5,
		title: 'Short Black Crew Neck T-Shirt',
		price: '20.21',
		img: IMAGES.PRODUCT4,
		subtitle: 'Product will be launched in 12h 53m',
		price: '20.21',
		description: 'Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more. Ac mauris mattis turpis malesuada. Eu lectus blandit ac laoreet blandit enim.Read more'
	}
]

const Search = ({navigation}) => {
	const thisDay = days.find(el => el.name === moment(new Date()).format('ddd'));
	const thisWeek = days.map(el => {
		return {
			...el,
			day: moment(new Date()).format('D') - el.id + thisDay.id
		}
	})

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
				<View style={styles.header}>
					<Text style={styles.month}>December, 2021</Text>
					<View style={styles.buttons}>
						<TouchableOpacity style={styles.button}>
							<Image source={ICONS.ARROW_LEFT}/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.button}>
							<Image source={ICONS.ARROW_RIGHT}/>
						</TouchableOpacity>
					</View>
				</View>
				<View style={styles.days}>
					{
						thisWeek.map(el => (
							<View key = {el.id} style={styles.day}>
								<Text style={styles.dayName}>{el.name}</Text>
								<View style={el.name === thisDay.name ? {...styles.dayNumber, ...styles.activeDayNumber} : styles.dayNumber}>
									<Text style={el.name === thisDay.name ? styles.activeDayNumberText : el.id < thisDay.id ? styles.afterDayNumberText : styles.dayNumberText}>{el.day}</Text>
								</View>
							</View>
						))
					}
				</View>
				<FlatList
					showsVerticalScrollIndicator={false}
					contentContainerStyle={styles.productsContainer}
					style={styles.products}
					data={products}
					keyExtractor={item => item.id}
					renderItem={({item}) =>
						<ProductItem 
							item={item}
							onPress={() =>
								navigation.navigate(MainFlowKeys.Product, {
									item: item,
								})
							}
						/>
					}
				/>	
			</View>
    </View>
  );
};

export default Search;
