import { StyleSheet } from "react-native";
import vars, {
  vScale,
  scale,
  FONT_FAMILY,
} from "../../constants/StylesConstants";
import { COLORS } from "../../constants/Colors";

export default StyleSheet.create({
  item: {
    borderRadius: scale(12),
    overflow: 'hidden',
    marginBottom: vScale(16)
  },
  image: {
    width: '100%',
    height: vScale(591),
  },
  gradient: {
    flex: 1,
    paddingHorizontal: scale(16),
    paddingTop: vScale(20),
    paddingBottom: vScale(16),
    justifyContent: 'space-between'
  },
  top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  labelWrapper: {
    width: scale(85),
    height: vScale(27),
    justifyContent: 'center',
    alignItems: 'center'
  },
  label: {
    color: COLORS.WHITE,
    fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
    fontSize: vars.fontSize.smallS
  },
  info: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  textWrapper: {
    width: scale(221)
  },
  subtitle: {
    color: COLORS.WHITE,
    fontFamily: FONT_FAMILY.MONTSERRAT_REGULAR,
    fontSize: vars.fontSize.smallS,
  },
  title: {
    marginTop: vScale(7),
    color: COLORS.WHITE,
    fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
    fontSize: vars.fontSize.largeS,
    lineHeight: vScale(30)
  },
  price: {
    color: COLORS.WHITE,
    fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
    fontSize: vars.fontSize.largeS,
    lineHeight: vScale(30)
  },
  buttons: {
    marginTop: vScale(31),
    flexDirection: 'row',
    justifyContent: 'space-between'
  }, 
  button: {
    justifyContent: 'center',
    paddingRight: scale(15)
  },
  buttonText: {
    color: COLORS.WHITE,
    fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
    fontSize: vars.fontSize.mediumS,
  }
});
