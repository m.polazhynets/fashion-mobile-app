import React, {useState} from "react";
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
	Image
} from "react-native";
import {LinearGradient} from 'expo-linear-gradient';
import { BlurView } from 'expo-blur';
import {Button} from '../index'

import {IMAGES, ICONS} from '../../constants/Images'
import styles from "./styles.js";

const NoveltiesItem = ({
  onPress = () => {},
  item
}) => {
  const [like, setLike] = useState(false)
  const {title, price, subtitle} = item;
  
  return (
    <TouchableOpacity
      style={styles.item}
      onPress={onPress}
    >
      <ImageBackground 
        style={styles.image} 
        source={IMAGES.BG_NOVELTIES} 
        resizeMode="cover"
    	>
        <LinearGradient
          style={styles.gradient}
					locations={[0.63, 1]}
          colors={['rgba(0, 0, 0, 0.06)', 'rgba(0, 0, 0, 0.75)']}
        >
					<View style={styles.top}>
						<BlurView intensity={25} tint="dark" style={styles.labelWrapper}>
							<Text style={styles.label}>This week</Text>
						</BlurView>
            <TouchableOpacity onPress={() => setLike(!like)}>
              {
                like ? <Image source={ICONS.RED_LIKE}/> : <Image source={ICONS.LIKE}/>
              }
            </TouchableOpacity>

					</View>

					<View style={styles.bottom}>
						<View style={styles.info}>
							<View style={styles.textWrapper}>
								<Text style={styles.subtitle}>{subtitle}</Text>
								<Text style={styles.title}>{title}</Text>
							</View>
							<Text style={styles.price}>${price}</Text>
						</View>
						<View style={styles.buttons}>
							<Button title='Pre-order'/>
							<TouchableOpacity style={styles.button}>
								<Text style={styles.buttonText}>Send as gift</Text>
							</TouchableOpacity>
						</View>
					</View>
        </LinearGradient>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default NoveltiesItem;
