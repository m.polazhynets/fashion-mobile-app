import React from "react";
import {
  Text,
  TouchableOpacity,
	Image
} from "react-native";

import styles from "./styles.js";

const ProductItem = ({
  onPress = () => {},
  item
}) => {
	const {title, price, img} = item
  
  return (
    <TouchableOpacity
      style={styles.item}
      onPress={onPress}
    >
			<Image source={img} style={styles.image}/>
			<Text style={styles.title}>{title}</Text>
			<Text style={styles.price}>${price}</Text>
    </TouchableOpacity>
  );
};

export default ProductItem;
