import { StyleSheet, Dimensions  } from "react-native";
import vars, {
  vScale,
  scale,
  FONT_FAMILY,
} from "../../constants/StylesConstants";
import { COLORS } from "../../constants/Colors";

export default StyleSheet.create({
  item: {
    width: Dimensions.get('window').width / 2  - 25,
		marginBottom: vScale(31)
  },
	price: {
		marginTop: vScale(8),
		color: COLORS.BEIGE,
		fontSize: vars.fontSize.smallL,
		fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
	},
	title: {
		color: COLORS.BLACK_LIGHT,
		fontSize: vars.fontSize.smallL,
		fontFamily: FONT_FAMILY.MONTSERRAT_MEDIUM,
		marginTop: vScale(8)
	}
});
