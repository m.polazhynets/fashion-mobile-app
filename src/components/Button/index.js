import React from "react";
import { Text, TouchableOpacity } from "react-native";
import styles from "./styles.js";

const Button = ({
  title,
  onPress = () => {},
  style
}) => {
  return (
    <TouchableOpacity
        style={{...styles.button, ...style}}
        onPress={onPress}
    >
        <Text style={styles.buttonText}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;
