import { StyleSheet } from "react-native";
import vars, {
  vScale,
  scale,
  FONT_FAMILY,
} from "../../constants/StylesConstants";
import { COLORS } from "../../constants/Colors";

export default StyleSheet.create({
  button: {
    width: scale(154),
    height: vScale(44),
    backgroundColor: COLORS.BEIGE,
    borderRadius: scale(4),
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
    fontSize: vars.fontSize.mediumS,
    color: COLORS.BLACK_LIGHT,
  },
});
