import { StyleSheet } from "react-native";
import vars, {
  vScale,
  scale,
  FONT_FAMILY,
} from "../../constants/StylesConstants";
import { COLORS } from "../../constants/Colors";

export default StyleSheet.create({
  button: {
    width: scale(30),
    height: scale(30),
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    borderRadius: scale(15),
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD,
    fontSize: vars.fontSize.mediumS,
    color: COLORS.BLACK_LIGHT,
  },
});
