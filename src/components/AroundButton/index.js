import React from "react";
import { Image, TouchableOpacity } from "react-native";
import styles from "./styles.js";

const AroundButton = ({
  icon,
  onPress = () => {},
  style
}) => {
  return (
    <TouchableOpacity
			style={{...styles.button, ...style}}
			onPress={onPress}
    >
      <Image source={icon} />
    </TouchableOpacity>
  );
};

export default AroundButton;
