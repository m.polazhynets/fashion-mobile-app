import React from "react";
import {Text, TouchableOpacity, ImageBackground} from "react-native";
import {LinearGradient} from 'expo-linear-gradient';

import {IMAGES} from '../../constants/Images'
import styles from "./styles.js";

const CategoriesItem = ({
  onPress = () => {},
  item
}) => {

  const {title} = item

  return (
    <TouchableOpacity
      style={styles.item}
      onPress={onPress}
    >
      <ImageBackground 
        style={styles.image} 
        source={IMAGES.BG_CATEGORY} 
        resizeMode="cover">
        <LinearGradient
          style={styles.gradient}
          colors={['rgba(18, 18, 18, 0)', 'rgba(18, 18, 18, 0.5)']}>
          <Text style={styles.title}>{title}</Text>
        </LinearGradient>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default CategoriesItem;
