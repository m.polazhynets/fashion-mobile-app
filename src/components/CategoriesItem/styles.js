import { StyleSheet } from "react-native";
import vars, {
  vScale,
  scale,
  FONT_FAMILY,
} from "../../constants/StylesConstants";
import { COLORS } from "../../constants/Colors";

export default StyleSheet.create({
  item: {
    marginRight: scale(20),
    borderRadius: scale(10),
    overflow: 'hidden'
  },
  image: {
    width: scale(280),
    height: vScale(78),
  },
  title: {
    color: COLORS.WHITE,
    fontSize: vars.fontSize.mediumS,
    fontFamily: FONT_FAMILY.MONTSERRAT_SEMI_BOLD
  },
  gradient: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
